<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {return view('welcome');
//});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('individaul', 'HomeController@index')->name('home');
Route::get('/',array('uses'=>'IndividaulController@home'));
Route::get('home',array('uses'=>'IndividaulController@home'));
Route::get('individaul',array('uses'=>'IndividaulController@individaul'));
Route::get('pay',array('uses'=>'IndividaulController@pay'));
